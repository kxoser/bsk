package tdd.training.bsk;

import java.util.ArrayList;
import java.util.List;

public class Game {
	
	ArrayList<Frame> frame;
	int firstBonusThrow;
	int secondBonusThrow;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		
		frame = new ArrayList<Frame>();
				
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		this.frame.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {

		if(index < 0 || index > 9) { throw new BowlingException(); }
		
		return frame.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {

		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {

		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int Score = 0;
		
		for(int i = 0; i < 10; i++) {
			
			if(isSpareInLastThrow(i)) {
				Score = Score + getFirstBonusThrow();
			}
			
			if(isSpareNotInLastThrow(i)) {
				frame.get(i).setBonus(frame.get(i+1).getFirstThrow());
				Score = Score + frame.get(i).getBonus();
			}
			
			if(isStrikeInLastThrow(i)) {
				Score = Score + getFirstBonusThrow() + getSecondBonusThrow();
			}
			
			if(isStrikeNotInLastThrow(i)) {
				
				if(isNextThrowStrike(i)) {
					
					if(isPenultimateThrow(i)) {
						frame.get(i).setBonus(frame.get(i+1).getFirstThrow() + 10);
					}else {
						frame.get(i).setBonus(frame.get(i+1).getFirstThrow() + frame.get(i+2).getFirstThrow());
					}
					
				}else {
					frame.get(i).setBonus(frame.get(i+1).getFirstThrow() + frame.get(i+1).getSecondThrow());
				}
				Score = Score + frame.get(i).getBonus();
			}
			
			Score = Score + frame.get(i).getFirstThrow() + frame.get(i).getSecondThrow();

		}

		return Score;	
	}
	
	public boolean isSpareInLastThrow(int index) {
		return (frame.get(index).isSpare() && index == 9);
	}

	public boolean isSpareNotInLastThrow(int index) {
		return (frame.get(index).isSpare() && index != 9);
	}
	
	public boolean isStrikeInLastThrow(int index) {
		return (frame.get(index).isStrike() && index == 9);
	}

	public boolean isStrikeNotInLastThrow(int index) {
		return (frame.get(index).isStrike() && index != 9);
	}
	
	public boolean isNextThrowStrike(int index) {
		return (frame.get(index+1).isStrike());
	}
	
	public boolean isPenultimateThrow(int index) {
		return (index==8);
	}
}
