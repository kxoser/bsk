package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testFrameFirstThrow() throws BowlingException{
		Frame frame = new Frame(5,5);
		assertEquals(5, frame.getFirstThrow());
	}
	
	@Test
	public void testFrameSecondThrow() throws BowlingException{
		Frame frame = new Frame(5,5);
		assertEquals(5, frame.getSecondThrow());
	}
	
	@Test
	public void testFrameScore() throws BowlingException{
		Frame frame = new Frame(5,5);
		assertEquals(10, frame.getScore());
	}
	
	@Test
	public void testFrameBonus() throws BowlingException{
		
		Frame frame = new Frame(3,6);
		frame.setBonus(frame.getFirstThrow());
		
		assertEquals(3, frame.getBonus());
	}
	
	@Test
	public void testFrameIsSpare() throws BowlingException{
		
		Frame frame = new Frame(1,9);
		
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testFrameIsNotSpare() throws BowlingException{
		
		Frame frame = new Frame(2,5);
		
		assertFalse(frame.isSpare());
	}
	
	@Test (expected = BowlingException.class)
	public void testFrameNotValid() throws BowlingException{
		
		Frame frame = new Frame(5,9);
		
	}
	
	@Test
	public void testFrameScoreWithSpare() throws BowlingException{
		Frame frame = new Frame(1,9);
		frame.setBonus(3);
		
		assertEquals(13, frame.getScore());
	}
	
	@Test
	public void testFrameIsStrike() throws BowlingException{
		
		Frame frame = new Frame(10,0);
		
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testFrameIsNotStrike() throws BowlingException{
		
		Frame frame = new Frame(0,10);
		
		assertFalse(frame.isStrike());
	}

	@Test
	public void testFrameScoreWithStrike() throws BowlingException{
		Frame frame = new Frame(10,0);
		frame.setBonus(9);
		
		assertEquals(19, frame.getScore());
	}
	
}
